all: main

main: main.cc
	$(CXX) -o $@ $^

check:
	echo clang-format-6.0 -style=Google main.cc
	clang-format-6.0 -style=Google main.cc

test: main
	./main
